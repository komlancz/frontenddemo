const pages = ['Users', 'Cars', 'Create'];

function Car(licensePlate, color, age, ownerName) {
  this.licensePlate = licensePlate;
  this.color = color;
  this.age = age;
  this.owner = {
    name: ownerName
  }
}

function UserViewModel() {
  var self = this;

  self.selectedPageData = ko.observableArray();
  self.activePage = ko.observable();
  self.subPage = ko.observable();
  self.addCarToUser = ko.observable(true);
  self.openedDialog = ko.observable();
  self.acceptedDialogs = ko.observableArray();

  self.goToUsers = function() {
    self.openDialog('usersDialog');
    $.get('http://185.208.224.122:8080/smartfront-demo/api/users/getAll', self.selectedPageData);
  };

  self.goToCars = function() {
    self.openDialog('carsDialog');
    $.get('http://185.208.224.122:8080/smartfront-demo/api/cars/getAll', self.selectedPageData);
  };

  self.goToCarOwner = function(car) {
    goToCarOwnerById(car.owner.id);
  };
  self.goToUserCars = function(user) {
    if (user.cars.length == 0) {
      self.openDialog('noCarDialog');
    } else {
      self.selectedPageData([]);
      self.activePage('Cars');
      $.get('http://185.208.224.122:8080/smartfront-demo/api/cars/getAllByOwner/' + user.id, self.selectedPageData);
    }
  };

  self.goToPage = function(page) {
    self.selectedPageData([]);
    self.activePage(page);

    if (page == 'Users') {
      self.goToUsers();
    } else if (page == 'Cars') {
      self.goToCars();
    }
  };

  $("#noCarDialog").dialog({
          modal: true,
          autoOpen: false,
          width: 400,
          minHeight: 150,
          maxHeight: 150,
          marginTop: 150,
          position: "center",
          resizable: false
      });

    $("#startDialog").dialog({
            modal: true,
            autoOpen: false,
            width: 400,
            minHeight: 150,
            maxHeight: 150,
            position: "center",
            resizable: false
        });


      $("#usersDialog").dialog({
              modal: true,
              autoOpen: false,
              width: 400,
              minHeight: 150,
              maxHeight: 150,
              position: "center",
              resizable: false
          });


      $("#carsDialog").dialog({
              modal: true,
              autoOpen: false,
              width: 400,
              minHeight: 150,
              maxHeight: 150,
              position: "center",
              resizable: false
          });

      self.cancelConfirm = function () {
          self.closeDialog();
      };
      self.cancelDeny = function () {
          alert("deny");
      };

      self.openDialog = function (dialogName) {
        if ("noCarDialog" == dialogName || !self.acceptedDialogs().includes(dialogName)) {
          $("#" + dialogName).dialog("open");
          self.openedDialog(dialogName);
        }
      };

      self.closeDialog = function () {
          $("#" + self.openedDialog()).dialog("close");
          self.acceptedDialogs().push(self.openedDialog());
          self.openedDialog();
      };


      self.username = ko.observable();
      self.userAge = ko.observable();
      self.newCarLicensePlate = ko.observable();
      self.newCarColor = ko.observable();
      self.newCarAge = ko.observable();

      self.pushCarToList = function(data) {
        self.selectedPageData.push(new Car(self.newCarLicensePlate(), self.newCarColor(), self.newCarAge(), self.username()));

        // clear details after push to list
        self.newCarLicensePlate(null);
        self.newCarColor(null);
        self.newCarAge(null);
      }

      self.saveUser = function() {
            $.ajax("http://185.208.224.122:8080/smartfront-demo/api/users/create", {
                data: ko.toJSON({
                  name: self.username,
                  age: self.userAge,
                  cars: self.selectedPageData
                 }),
                type: "post", contentType: "application/json",
                success: function(result) {
                  self.username(null);
                  self.userAge(null);
                  self.addCarToUser(false);
                  var usersResponse = result;
                  var newUserId = result.id;
                  alert('Saved id: ' + newUserId);
                  goToCarOwnerById(newUserId);
                },
                error: function(result) {alert('Something went wrong!')}
            });
        };

    self.openDialog('startDialog');

    function goToCarOwnerById(id) {
      self.selectedPageData([]);
      self.activePage('Users');
      $.get('http://185.208.224.122:8080/smartfront-demo/api/users/' + id, self.selectedPageData);
    }
}

ko.applyBindings(new UserViewModel());
